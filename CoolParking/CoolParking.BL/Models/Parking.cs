﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        private static Parking instance;
        public decimal ParkingBalance { get; set; } 
        public List<Vehicle> Vehicles { get; set; }
        public int Capacity { get; set; }
        private Parking(decimal parkingBalance, int capasity)
        {
            ParkingBalance = parkingBalance;
            Capacity = capasity;
            Vehicles = new List<Vehicle>();
        }
        public static Parking GetParking()
        {
            if (instance == null)
            {
                instance = new Parking(Settings.InitalBalance, Settings.Capacity);
                return instance;
            }
            else
            {
                return instance;
            }
        }

        public void Dispose()
        {
            instance = null;
        }
    }
}
