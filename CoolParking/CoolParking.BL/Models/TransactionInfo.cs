﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public decimal Sum { get; set; }
        public DateTime Time { get; set; }
        public string VehicleId { get; set; }
        public TransactionInfo(decimal sum, DateTime time, string vehicleId)
        {
            Sum = sum;
            Time = time;
            VehicleId = vehicleId;
        }
        public override string ToString()
        {
            return $"Transaction time: {Time.ToLocalTime()}; vehicle ID: {VehicleId}; withdrawn sum: {Sum}";
        }
    }
}