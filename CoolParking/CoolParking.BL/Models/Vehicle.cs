﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (Regex.IsMatch(id, @"[A-Z]{2}-\d{4}-[A-Z]{2}"))
            {
                Id = id;
            }
            else
            {
                throw new ArgumentException("Plate must correspond format ХХ-YYYY-XX, where X - uppercase letter and Y - digit");
            }

            if (balance > 0)
            {
                Balance = balance;
            }
            else
            {
                throw new ArgumentException("Initial balance must be greater than 0");
            }

            VehicleType = vehicleType;
        }

        public void TopUp(decimal sum)
        {
            Balance += sum;
        }

        public void Withdraw(decimal sum)
        {
            Balance -= sum;
        }

        public decimal GetBalance()
        {
            return Balance;
        }

        public string GetVehicleId()
        {
            return Id;
        }


        public VehicleType GetVehicleType()
        {
            return VehicleType;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            string randomID = String.Empty;
            Random random = new Random();
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string digits = "0123456789";
            string RandomString(string symbols, int length)
            {
                return new string(Enumerable.Repeat(symbols, length)
                  .Select(s => s[random.Next(s.Length)]).ToArray());
            }

            randomID += RandomString(letters, 2);
            randomID += "-";
            randomID += RandomString(digits, 4);
            randomID += "-";
            randomID += RandomString(letters, 2);

            return randomID;
    }


    }
}
