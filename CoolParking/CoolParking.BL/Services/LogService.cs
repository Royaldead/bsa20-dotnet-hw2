﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }
        public LogService()
        { }
        public LogService(string path)
        {
            LogPath = path;
        }
        public string Read()
        {
            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (FileNotFoundException)
            {
                throw new InvalidOperationException("File is not found");
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void Write(string logInfo)
        {
            using (StreamWriter streamWriter = File.AppendText(LogPath))
            {
                streamWriter.WriteLine(logInfo);
            }
        }
    }
}
