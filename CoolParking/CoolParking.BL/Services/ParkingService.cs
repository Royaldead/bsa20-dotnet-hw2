﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Threading;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;

        public Parking Parking { get; set; }
        private List<TransactionInfo> LastParkingTransactions { get; set; }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;

            _withdrawTimer.Elapsed += Withdraw;
            _withdrawTimer.Interval = Settings.ChargeInterval*1000;
            _withdrawTimer.Start();

            _logService = logService;
            Thread.Sleep(100);

            _logTimer = logTimer;
            _logTimer.Elapsed += Log;
            _logTimer.Interval = Settings.LogInterval*1000;
            _logTimer.Start();

            Parking = Parking.GetParking();
            LastParkingTransactions = new List<TransactionInfo>();
        }
        public void AddVehicle(Vehicle vehicle)
        {
            List<Vehicle> parkingVehicles = Parking.Vehicles;
            if (parkingVehicles.FindIndex(v => v.Id == vehicle.Id) > -1)
            {
                throw new ArgumentException("Vehicle wuth given Id has already on parking");
            }
            if (parkingVehicles.Count < Settings.Capacity)
            {
                parkingVehicles.Add(vehicle);
            }
            else
            {
                throw new InvalidOperationException("Sorry, parking is full");
            }
        }

        public void Dispose()
        {
            _withdrawTimer.Stop();
            _logTimer.Stop();

            _withdrawTimer.Dispose();
            _logTimer.Dispose();

            Parking.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.ParkingBalance;
        }

        public int GetCapacity()
        {
            return Parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Capacity - Parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return LastParkingTransactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return Parking.Vehicles.AsReadOnly();
        }

        public string ReadFromLog()
        {
            try
            {
                return _logService.Read();
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            List<Vehicle> parkingVehicles = Parking.Vehicles;
            int vehicleIndex = parkingVehicles.FindIndex(v => v.Id == vehicleId);
            if (vehicleIndex == -1)
            {
                throw new ArgumentException("Vehicle with given Id is not in parking");
            }
            else if (parkingVehicles[vehicleIndex].GetBalance() < 0)
            {
                throw new InvalidOperationException("Sorry, you must pay a debt first. Please, top up your balance.");
            }
            else
            {
                parkingVehicles.RemoveAt(vehicleIndex);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {

            List<Vehicle> parkingVehicles = Parking.Vehicles;
            Vehicle targetVehicle = parkingVehicles.Find(v => v.Id == vehicleId);
            if (targetVehicle == null)
            {
                throw new ArgumentException("Vehicle with given Id is not in parking");
            }
            else if (sum < 0)
            {
                throw new ArgumentException("Top up sum must be greater than 0");
            }
            else
            {
                targetVehicle.TopUp(sum);
            }
        }

        public void Log(object sender, ElapsedEventArgs e)
        {

                TransactionInfo[] trancactions = GetLastParkingTransactions();

                string logHeader = $"\nTime of log: {DateTime.Now}; transaction entries count: {trancactions.Length}";
                _logService.Write(logHeader);
                foreach (var transaction in trancactions)
                {
                    _logService.Write(transaction.ToString());
                }
                LastParkingTransactions.Clear();

        }

        public void Withdraw(object sender, ElapsedEventArgs e)
        {
                var vehiclesOnParking = Parking.Vehicles;
                foreach (var vehicle in vehiclesOnParking)
                {
                    decimal rate = GetVehicleRate(vehicle.VehicleType);
                    decimal vehicleBalance = vehicle.GetBalance();
                    if (vehicleBalance > 0 && vehicleBalance - rate >= 0)
                    {
                        decimal sum = rate;
                        ExecuteTransaction(vehicle, sum);
                    }
                    else if (vehicleBalance > 0 && vehicleBalance - rate < 0)
                    {
                        decimal sum = (rate - vehicleBalance) * Settings.PenaltyMultiplier + vehicleBalance;
                        ExecuteTransaction(vehicle, sum);
                    }
                    else
                    {
                        decimal sum = rate * Settings.PenaltyMultiplier;
                        ExecuteTransaction(vehicle, sum);
                    }
                }
        }

        private void ExecuteTransaction(Vehicle vehicle, decimal sum)
        {
            vehicle.Withdraw(sum);
            Parking.ParkingBalance += sum;
            TransactionInfo parkingTransaction = new TransactionInfo() 
            { 
                Time = DateTime.Now,
                Sum = sum,
                VehicleId = vehicle.Id
            };
            LastParkingTransactions.Add(parkingTransaction);
        }

        private decimal GetVehicleRate(VehicleType vehicleType)
        {
            switch (vehicleType)
            {
                case VehicleType.PassengerCar:
                    return Settings.PassengerCarRate;

                case VehicleType.Bus:
                    return Settings.BusRate;

                case VehicleType.Motorcycle:
                    return Settings.MotorcycleRate;

                case VehicleType.Truck:
                    return Settings.TruckRate;

                default:
                    throw new ArgumentException("Cannot calculate rate");
            }
        }
        public decimal GetProfitSinceLastLog()
        {
            return GetLastParkingTransactions().ToList().Sum(tr => tr.Sum);
        }

    }
}
