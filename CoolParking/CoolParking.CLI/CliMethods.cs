﻿using Castle.Core.Internal;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.CLI
{
    static class CliMethods
    {
        public static void ChooseAndExecuteMethod(ParkingService parkingService, int intAnswer)
        {
            switch (intAnswer)
            {
                case 1:
                    ShowParkingBalance(parkingService);
                    break;
                case 2:
                    ShowProfitSinceLastLog(parkingService);
                    break;
                case 3:
                    ShowFreeParkingSpaces(parkingService);
                    break;
                case 4:
                    ShowTransactionsSinceTheLastLog(parkingService);
                    break;
                case 5:
                    ShowAllTimeTransactions(parkingService);
                    break;
                case 6:
                    ShowVehiclesInTheParking(parkingService);
                    break;
                case 7:
                    PutVehicleInTheParking(parkingService);
                    break;
                case 8:
                    RemoveVehicleFromTheParking(parkingService);
                    break;
                case 9:
                    TopUpYourBalance(parkingService);
                    break;
                default:
                    break;
            }
        }

        private static void TopUpYourBalance(ParkingService parkingService)
        {
            string vehicleId;
            decimal balance;
            vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                return;
            }
            balance = GetBalance();
            if (balance == decimal.MinValue)
            {
                return;
            }
            try
            {
                parkingService.TopUpVehicle(vehicleId, balance);
                Console.WriteLine($"\nYour balance has been topped up by {balance}");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"\n{exception.Message}");
                return;
            }
        }

        private static void RemoveVehicleFromTheParking(ParkingService parkingService)
        {
            string vehicleId;
            vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                return;
            }
            try
            {
                parkingService.RemoveVehicle(vehicleId);
                Console.WriteLine("\nVehicle removed");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"\n{exception.Message}");
                return;
            }
            
        }

        private static void PutVehicleInTheParking(ParkingService parkingService)
        {
            string vehicleId;
            VehicleType vehicleType;
            decimal balance;

            vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                return;
            }

            int enumValue = GetVehicleType();
            if (enumValue == Int32.MinValue)
            {
                return;
            }
            else
            {
                vehicleType = (VehicleType)enumValue;
            }

            balance = GetBalance();
            if (balance == decimal.MinValue)
            {
                return;
            }

            try
            {
                Vehicle vehicle = new Vehicle(vehicleId, vehicleType, balance);
                parkingService.AddVehicle(vehicle);
                Console.WriteLine("\nVehicle added");
            }
            catch (Exception exception)
            {
                Console.WriteLine($"\n{exception.Message}");
                return;
            }
                
        }

        private static void ShowVehiclesInTheParking(ParkingService parkingService)
        {
            var vehicles = parkingService.GetVehicles();
            if (vehicles.ToList().Count == 0)
            {
                Console.WriteLine("\nThere are no vehicles in the parking now");
                return;
            }
            Console.WriteLine("\nThere are vehicles with such ID's in the parking now:\n");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"ID: {vehicle.GetVehicleId()}, type: {vehicle.GetVehicleType()}");
            }
        }

        private static void ShowAllTimeTransactions(ParkingService parkingService)
        {
            try
            {
                string logFile = parkingService.ReadFromLog();
                if (logFile.IsNullOrEmpty())
                {
                    Console.WriteLine("\nFile Transactions.log is empty");
                    return;
                }
                Console.WriteLine("\n*** The content of Transactions.log ***");
                Console.WriteLine(logFile);   
                
            }
            catch (FileNotFoundException exception)
            {
                Console.WriteLine($"\n{exception.Message}");
                return;
            }
        }

        private static void ShowTransactionsSinceTheLastLog(ParkingService parkingService)
        {
            List<TransactionInfo> lastTransactions = parkingService.GetLastParkingTransactions().ToList();
            if (lastTransactions.Count == 0)
            {
                Console.WriteLine("\nThere are no transactions since the last log");
                return;
            }
            Console.WriteLine($"\n*** Transactions since the last log: ***");
            foreach (var transaction in lastTransactions)
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        private static void ShowFreeParkingSpaces(ParkingService parkingService)
        {
            Console.WriteLine($"\nThere are {parkingService.GetFreePlaces()} free spaces of {parkingService.GetCapacity()} in the parking");
        }

        private static void ShowProfitSinceLastLog(ParkingService parkingService)
        {
            decimal profit = parkingService.GetProfitSinceLastLog();
            Console.WriteLine($"\nParking profit since last log: {profit}");
        }

        private static void ShowParkingBalance(ParkingService parkingService)
        {
            Console.WriteLine($"\nCurrent parking balance: {parkingService.GetBalance()}");
        }

        private static string GetVehicleId()
        {
            while (true)
            {
                Console.WriteLine("\nType vehicle ID (plate number) in format ХХ-YYYY-XX,\nwhere X - letter and Y - digit or type 'q' and press 'Enter' to exit:\n");
                string stringAnswer = Console.ReadLine().ToUpper();

                if (stringAnswer == "Q")
                {
                    Console.WriteLine("\nExiting...");
                    return null;
                }
                else if (!Regex.IsMatch(stringAnswer, @"[A-Z]{2}-\d{4}-[A-Z]{2}"))
                {
                    Console.WriteLine("\nWrong vehicle ID has been typed. Please, try again.");
                    continue;
                }
                else
                {
                    return stringAnswer;
                }
            }
        }

        private static int GetVehicleType()
        {
            while (true)
            {
                Console.WriteLine("\nChoose your vehicle type (type a digit and press 'Enter') or type 'q' and press 'Enter' to exit");
                Console.WriteLine("0. Passenger Car");
                Console.WriteLine("1. Truck");
                Console.WriteLine("2. Bus");
                Console.WriteLine("3. Motorcycle\n");
                string stringAnswer = Console.ReadLine().ToUpper();

                if (stringAnswer == "Q")
                {
                    Console.WriteLine("\nExiting...");
                    return Int32.MinValue;
                }

                bool parseAnswerSuccess = Int32.TryParse(stringAnswer, out int intAnswer);

                if (!parseAnswerSuccess || intAnswer < 0 || intAnswer > 3)
                {
                    Console.WriteLine("\nWrong vehicle type has been typed. Please, try again.");
                    continue;
                }
                else
                {
                    return intAnswer;
                }
            }
        }

        private static decimal GetBalance()
        {
            while (true)
            {
                Console.WriteLine("\nType a balance in format X.XX,\nwhere X - digit (must be greater than 0) or type 'q' and press 'Enter' to exit\n");
                string stringAnswer = Console.ReadLine().ToUpper();

                if (stringAnswer == "Q")
                {
                    Console.WriteLine("\nExiting...");
                    return decimal.MinValue;
                }

                bool parseAnswerSuccess = Decimal.TryParse(stringAnswer, out decimal decimalAnswer);

                int numberOfDecimalPlaces = parseAnswerSuccess ? BitConverter.GetBytes(decimal.GetBits(decimalAnswer)[3])[2] : Int32.MaxValue;

                if (!parseAnswerSuccess || decimalAnswer <= 0 || numberOfDecimalPlaces > 2)
                {
                    Console.WriteLine("\nWrong balance has been typed. Please, try again.");
                    continue;
                }
                else
                {
                    return decimalAnswer;
                }
            }
        }
    }
}
